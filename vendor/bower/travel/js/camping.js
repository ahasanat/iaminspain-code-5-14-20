var currency_array = ["USD", "EUR", "YEN", "CAD", "AUE"];
var guests_array = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "100"];
var services_array = ['Waste tank discharge', 'Public lavatory', 'Walking path', 'Swimming pool', 'Fishing permits', 'Cooking facilities', 'Sports hall', 'Washing machine', 'Hot pot', 'Sports field', 'Shower', 'Golf course', 'Sauna', 'Play ground'];

$(document).ready(function() {
  justifiedGalleryinitialize();
  lightGalleryinitialize();

  $('input[name="datetimes"]').daterangepicker({
    opens: 'center',
    autoApply: true,
    autoUpdateInput: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'M/DD'
    }
  });
});

$(document).ready(function(){ 
  $w = $(window).width(); 
  if ( $w > 739) {      
    $(".places-tabs .sub-tabs li a").click(function(){
       $("body").removeClass("remove_scroller");
    }); 
    $(".tabs.icon-menu.tabsnew li a").click(function(){
       $("body").removeClass("remove_scroller");
    }); 
    $(".mbl-tabnav").click(function(){
       $("body").removeClass("remove_scroller");
    }); 
    $(".clicable.viewall-link").click(function(){
       $("body").removeClass("remove_scroller");
    }); 
  } else {
    $(".places-tabs .sub-tabs li a").click(function(){
       $("body").addClass("remove_scroller");
    }); 
    $(".clicable.viewall-link").click(function(){
       $("body").addClass("remove_scroller");
    });         
    $(".tabs.icon-menu.tabsnew li a").click(function(){
       $("body").addClass("remove_scroller");
    }); 
    $(".mbl-tabnav").click(function(){
       $("body").removeClass("remove_scroller");
    });
  }

  $(".header-icon-tabs .tabsnew .tab a").click(function(){
    $(".bottom_tabs").hide();
  });

  $(".places-tabs .tab a").click(function(){
    $(".top_tabs").hide();
  });

  // footer work for places home page only
  $('.footer-section').css('left', '0');
  $w = $(window).width();
  if($w <= 768) {
    $('.main-footer').css({
       'width': '100%',
       'left': '0'
    });
  } else {
    var $_I = $('.places-content.places-all').width();
    var $__I = $('.places-content.places-all').find('.container').width();

    var $half = parseInt($_I) - parseInt($__I);
    $half = parseInt($half) / 2;

    $('.main-footer').css({
       'width': $_I+'px',
       'left': '-'+$half+'px'
    });
  }
  $('.camping-page').find('.collection-card-inner').find('img').css('opacity', '1');
});

$(window).resize(function() {
  // footer work for places home page only
  if($('#places-all').hasClass('active')) {
    $('.footer-section').css('left', '0');
    $w = $(window).width();
    if($w <= 768) {
       $('.main-footer').css({
          'width': '100%',
          'left': '0'
       });
    } else {
       var $_I = $('.places-content.places-all').width();
       var $__I = $('.places-content.places-all').find('.container').width();

       var $half = parseInt($_I) - parseInt($__I);
       $half = parseInt($half) / 2;

       $('.main-footer').css({
          'width': $_I+'px',
          'left': '-'+$half+'px'
       });
    }
  }
});

$(document).on('click', '.tablist .tab a', function(e) {
  $href = $(this).attr('href');
  $href = $href.replace('#', '');

  $('.places-content').removeClass().addClass('places-content '+$href);
  $this = $(this);
});

$(document).on('click', '.createcamping', function () {
  if($(this).hasClass('checkuserauthclassnv')) {  
    checkuserauthclassnv();
  } else if($(this).hasClass('checkuserauthclassg')) {
    checkuserauthclassg();
  } else {
    $.ajax({
      url: '?r=camping/createcamping',
      success: function(data){
        $('#campCreateModal').html(data);
        setTimeout(function() { 
            $('#campCreateModal').modal('open'); 
            initDropdown();
            $('input[name="datetimes"]').daterangepicker({
              opens: 'center',
              autoApply: true,
              autoUpdateInput: true,
              startDate: moment().startOf('hour'),
              endDate: moment().startOf('hour').add(32, 'hour'),
              locale: {
                format: 'M/DD'
              }
            });

            $('input[name="datetimes"]').on('showCalendar.daterangepicker', function(ev, picker) {
              $modal_width = $('#campCreateModal').width();
              console.log('$modal_width '+$modal_width);

              $modal_height = $('#campCreateModal').height();
              console.log('$modal_height '+$modal_height);

              $window_width = $(window).width();
              console.log('$window_width '+$window_width);

              $window_height = $(window).height();
              console.log('$window_height '+$window_height);


/*              $('.daterangepicker').css("cssText", "width: 480px !important;");
              $('.daterangepicker').css("cssText", "height: 596px !important;");*/

              var $left = ($window_width - $modal_width) / 2;
              console.log('$left '+$left);

              var $top = ($window_height - $modal_height) / 2;
              console.log('$top '+$top);

              $('.daterangepicker').css("cssText", "left: 78.5px !important;");
              $('.daterangepicker').css("cssText", "top: 30.5px !important;");
            });            

        }, 400);
      }
    });
  }
});

$('input[name="datetimes"]').on('showCalendar.daterangepicker', function(ev, picker) {
  console.log('ARE THAYU?;');
});

$(document).on('click', '.editcamping', function () {
  if($(this).hasClass('checkuserauthclassnv')) {  
    checkuserauthclassnv();
  } else if($(this).hasClass('checkuserauthclassg')) {
    checkuserauthclassg();
  } else {

    $id = $(this).attr('data-editid');
    if($id) {
      $.ajax({
        url: '?r=camping/editcamping',
        type: 'POST',
        data: {id: $id},
        success: function(data){
          $('#campEditModal').html(data);
            setTimeout(function() { 
                $('#campEditModal').modal('open'); 
                initDropdown();
                $('input[name="datetimes"]').daterangepicker({
                  opens: 'center',
                  autoApply: true,
                  autoUpdateInput: true,
                  startDate: moment().startOf('hour'),
                  endDate: moment().startOf('hour').add(32, 'hour'),
                  locale: {
                    format: 'M/DD'
                  }
                });

                $('input[name="datetimes"]').on('showCalendar.daterangepicker', function(ev, picker) {
                  console.log('Hello RMANI');
                });

                console.log('ds');
            }, 400);
        }
      });
    }
  }
});



$(document).on('click', '.campingreview', function (e) {
  if($(this).hasClass('checkuserauthclassnv')) {
    checkuserauthclassnv();
  } else if($(this).hasClass('checkuserauthclassg')) {
    checkuserauthclassg();
  } else {
    $.ajax({
          url: '?r=camping/review',
          success: function(data){
            $('#camping_review').html(data);
              setTimeout(function() { 
                  initDropdown();
                  $('.tabs').tabs();
                  $('#camping_review').modal('open');
              }, 400);
          }
      });
  }
});

function createcampingsave() {
  applypostloader('SHOW');
  var validate = true;
  
  var camping_title = $('#camping_title').val();
  var camping_minguests = $('#camping_minguests').find('span').text().trim();
  var camping_maxguests = $('#camping_maxguests').find('span').text().trim();
  var camping_rate = $("#camping_rate").val();
  var camping_currency = $("#camping_currency").find('span').text().trim();
  var camping_description = $("#camping_description").val();
  var camping_location = $("#camping_location").val();
  var camping_telephone = $("#camping_telephone").val();
  var camping_email = $("#camping_email").val(); 
  var camping_website = $("#camping_website").val();
  var camping_period = $("#camping_period").val();

  var services = [];
  $.each($("#camping_services").find('.check-image'), function(){            
    if($(this).hasClass('active-class')) {
      $s_ = $(this).find('img').attr('title');
      if(jQuery.inArray($s_, services_array) !== -1) {
          services.push($s_);
      }
    }
  });

  if (camping_title == null || camping_title == undefined || camping_title == '') {
    validate = false;
    Materialize.toast('Enter title.', 2000, 'red');
    $('#camping_title').focus();
    applypostloader('HIDE');
    return false;
  }

  if(guests_array.indexOf(camping_minguests) !== -1){
  } else {
    validate = false;
    Materialize.toast('Something is wrong.', 2000, 'red');
    applypostloader('HIDE');
    return false;    
  }

  if(guests_array.indexOf(camping_maxguests) !== -1){
  } else {
    validate = false;
    Materialize.toast('Something is wrong.', 2000, 'red');
    applypostloader('HIDE');
    return false;    
  }

  if (camping_rate == null || camping_rate == undefined || camping_rate == '') {
    validate = false;
    Materialize.toast('Enter rate.', 2000, 'red');
    $("#camping_rate").focus();
    applypostloader('HIDE');
    return false;
  }

  if (camping_currency == null || camping_currency == undefined || camping_currency == '') {
    validate = false;
    Materialize.toast('Select currency.', 2000, 'red');
    $("#camping_rate").focus();
    applypostloader('HIDE');
    return false;
  }
  if(currency_array.indexOf(camping_currency) !== -1){
  } else {
    validate = false;
    Materialize.toast('Something is wrong in currency.', 2000, 'red');
    $("#camping_rate").focus();
    applypostloader('HIDE');
    return false;    
  }

  if (camping_description == null || camping_description == undefined || camping_description == '') {
    validate = false;
    Materialize.toast('Enter description.', 2000, 'red');
    $("#camping_description").focus();
    applypostloader('HIDE');
    return false;
  }

  if (camping_location == null || camping_location == undefined || camping_location == '') {
    validate = false;
    Materialize.toast('Enter location.', 2000, 'red');
    $("#camping_location").focus();
    applypostloader('HIDE');
    return false;
  }

  /*$.each(services, function (i, v) {
    console.log(i);
    console.log(v);
    if(v.indexOf(services_array) !== -1){
    } else {
      validate = false;
      Materialize.toast('Something is wrong in services.', 2000, 'red');
      return false;    
    }
  });*/

  if (storedFiles.length < 3) {
    validate = false;
    Materialize.toast('Please upload three cover photos.', 2000, 'red');
    applypostloader('HIDE');
    return false;
  }

  var fd;
  fd = new FormData();
  for(var i=0, len=storedFiles.length; i<len; i++) {
      fd.append('camping_images[]', storedFiles[i]);
  }
  fd.append('camping_title', camping_title);
  fd.append('camping_minguests', camping_minguests);
  fd.append('camping_maxguests', camping_maxguests);
  fd.append('camping_rate', camping_rate);
  fd.append('camping_currency', camping_currency);
  fd.append('camping_description', camping_description);
  fd.append('camping_location', camping_location);
  fd.append('camping_telephone', camping_telephone);
  fd.append('camping_email', camping_email);
  fd.append('camping_website', camping_website);
  fd.append('camping_period', camping_period);
  fd.append('camping_services', services);

  if(validate) {
    $.ajax({ 
      url: '?r=camping/createcampingsave',
      type: 'POST',
      data: fd,
      contentType: 'multipart/form-data',
      processData: false,
      contentType: false,
      success: function(result) {
        //applypostloader('HIDE');
        $result = JSON.parse(result);
        if($result.status != undefined && $result.status == true) {
          $('#campCreateModal').modal('close'); 
          Materialize.toast('Camping created.', 2000, 'green');
          window.location.href="";
        }
      }
    });
  }
}

function editcampingsave($id, obj) {  
  applypostloader('SHOW');
  var validate = true;
  
  var camping_title = $('#editcamping_title').val();
  var camping_minguests = $('#editcamping_minguests').find('span').text();
  var camping_maxguests = $('#editcamping_maxguests').find('span').text();
  var camping_rate = $("#editcamping_rate").val();
  var camping_currency = $("#editcamping_currency").find('span').text();
  var camping_description = $("#editcamping_description").val();
  var camping_location = $("#editcamping_location").val();
  var camping_telephone = $("#editcamping_telephone").val();
  var camping_email = $("#editcamping_email").val();
  var camping_website = $("#editcamping_website").val();
  var camping_period = $("#editcamping_period").val();

  var services = [];
  $.each($("#editcamping_services").find('.check-image'), function(){            
    if($(this).hasClass('active-class')) {
      $s_ = $(this).find('img').attr('title');
      if(jQuery.inArray($s_, services_array) !== -1) {
          services.push($s_);
      }
    }
  });

  if (camping_title == null || camping_title == undefined || camping_title == '') {
    validate = false;
    Materialize.toast('Enter title.', 2000, 'red');
    $('#editcamping_title').focus();
    applypostloader('HIDE');
    return false;
  }

  if(guests_array.indexOf(camping_minguests) !== -1){
  } else {
    validate = false;
    Materialize.toast('Something is wrong.', 2000, 'red');
    applypostloader('HIDE');
    return false;    
  }

  if(guests_array.indexOf(camping_maxguests) !== -1){
  } else {
    validate = false;
    Materialize.toast('Something is wrong.', 2000, 'red');
    applypostloader('HIDE');
    return false;    
  }

  if (camping_rate == null || camping_rate == undefined || camping_rate == '') {
    validate = false;
    Materialize.toast('Enter rate.', 2000, 'red');
    $("#editcamping_rate").focus();
    applypostloader('HIDE');
    return false;
  }

  if (camping_currency == null || camping_currency == undefined || camping_currency == '') {
    validate = false;
    Materialize.toast('Select currency.', 2000, 'red');
    $("#editcamping_rate").focus();
    applypostloader('HIDE');
    return false;
  }
  if(currency_array.indexOf(camping_currency) !== -1){
  } else {
    validate = false;
    Materialize.toast('Something is wrong in currency.', 2000, 'red');
    $("#editcamping_rate").focus();
    applypostloader('HIDE');
    return false;    
  }

  if (camping_description == null || camping_description == undefined || camping_description == '') {
    validate = false;
    Materialize.toast('Enter description.', 2000, 'red');
    $("#editcamping_description").focus();
    applypostloader('HIDE');
    return false;
  }

  if (camping_location == null || camping_location == undefined || camping_location == '') {
    validate = false;
    Materialize.toast('Enter location.', 2000, 'red');
    $("#editcamping_location").focus();
    applypostloader('HIDE');
    return false;
  }

/*  $.each(services, function (i, v) {
    if(v.indexOf(services_array) !== -1){
    } else {
      validate = false;
      Materialize.toast('Something is wrong in services.', 2000, 'red');
      return false;    
    }
  });
*/
  if ($('#campEditModal').find('.custom-file').length) {
    validate = false;
    Materialize.toast('Please upload three cover photos.', 2000, 'red');
    applypostloader('HIDE');
    return false;
  } else if ($('#campEditModal').find('.post-photos').find('.img-row').find('.img-box').length != 3) {
    validate = false;
    Materialize.toast('Please upload three cover photos.', 2000, 'red');
    applypostloader('HIDE');
    return false;
  }


  var fd;
  fd = new FormData();
  for(var i=0, len=storedFiles.length; i<len; i++) {
      fd.append('camping_images[]', storedFiles[i]);
  }
  fd.append('camping_title', camping_title);
  fd.append('camping_minguests', camping_minguests);
  fd.append('camping_maxguests', camping_maxguests);
  fd.append('camping_rate', camping_rate);
  fd.append('camping_currency', camping_currency);
  fd.append('camping_description', camping_description);
  fd.append('camping_location', camping_location);
  fd.append('camping_telephone', camping_telephone);
  fd.append('camping_email', camping_email);
  fd.append('camping_website', camping_website);
  fd.append('camping_period', camping_period);
  fd.append('camping_services', services);
  fd.append('camping_id', $id);

  if(validate) {
    $.ajax({
      url: '?r=camping/editcampingsave',
      type: 'POST',
      data: fd,
      contentType: 'multipart/form-data',
      processData: false,
      contentType: false,
      success: function(result) {
        //applypostloader('HIDE');
        $result = JSON.parse(result);
        if($result.status != undefined && $result.status == true) {
          $('#campEditModal').modal('close'); 
          Materialize.toast('Camping updated.', 2000, 'green');
          window.location.href="";
        }
      }
    });
  }
}

function removepiccamping_modal($id, obj)
{   
    $this = $(obj);
    var $src = $this.parents('.img-box').find('img').attr('src');
    if($id) {   
        $.ajax({
            type: 'POST', 
            url: '?r=camping/removepic',  
            data: {$id, $src},
            success: function(data) {
              var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    Materialize.toast('Deleted', 2000, 'green');
                    $this.parents('.img-box').remove();

                    // Remove upload image input if 3 images is exists or uploaded....
                    if($('.modal.open').find('.upldimg').length >= 3) {
                        $('.modal.open').find('.custom-file').parents('.img-box').remove();
                    } else {
                        if(!$('.modal.open').find('.custom-file').length) {
                            $uploadBox = '<div class="img-box"> <div class="custom-file addimg-box add-photo ablum-add"> <span class="icont">+</span> <br><span class="">Update photo</span> <input class="upload custom-upload remove-custom-upload edit-collections-file-upload" id="edit-collections-file-upload" title="Choose a file to upload" data-class=".post-photos .img-row" type="file"> </div> </div>';
                            if($('.modal.open').find('.img-row').append($uploadBox));
                        }   
                    }
                }
            }
        });
    }
}

function addCampingReview() { 
  applypostloader('SHOW');
  var $select = $('#camping_review');

  var $w = $(window).width();
  var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
    var title = $select.find("#title").val();
    var status = $select.find('#textInput').val();
    
    if (status != "" && reg.test(status) == true) {
         status = status.replace(reg, " ");
    }
    if (title != "" && reg.test(title) == true) {
         title = title.replace(reg, " ");
    }

    var formdata;
    formdata = new FormData($('form')[1]);

    formdata.append("test", status);
    
    if($w > 568) {
      $post_privacy = $select.find('#post_privacy').text().trim();
  } else {
    $post_privacy = $select.find('#post_privacy2').text().trim();
  }

    if($.inArray($post_privacy, $post_privacy_array) > -1) {
  } else {
    $post_privacy = 'Public';
  }
    var $share_setting = 'Enable';
    var $comment_setting = 'Enable';
    if($('#sharing_setting_btns').find('.toolbox_disable_sharing').is(':checked')) {
      $share_setting = 'Disable';
    }
    if($('#sharing_setting_btns').find('.toolbox_disable_comments').is(':checked')) {
      $comment_setting = 'Disable';
    }

    $link_title = $('#link_title').val();
    $link_url = $('#link_url').val();
    $link_description = $('#link_description').val();
    $link_image = $('#link_image').val();
   
    var $totalStartFill = $select.find('.rating-stars').find('i.active').length;
  if($totalStartFill <=0) {
    Materialize.toast('Rate your self.', 2000, 'red');
    applypostloader('HIDE');
    return false;
  }

  $(".post_loadin_img").css("display","inline-block");
  
  formdata.append("posttags", addUserForTag);
  formdata.append("post_privacy", $post_privacy);
  formdata.append("link_title",$link_title);
  formdata.append("link_url",$link_url);
  formdata.append("link_description",$link_description);
  formdata.append("link_image",$link_image);
  formdata.append("title", title);
  formdata.append("share_setting",$share_setting);
  formdata.append("comment_setting",$comment_setting);
  formdata.append('custom', customArray);
  //formdata.append("current_location",place); 
  formdata.append("placereview", $totalStartFill);
  $.ajax({
    url: '?r=camping/addreview',
    type: 'POST',
    data:formdata,
    async:false,        
    processData: false,
    contentType: false,
    success: function(data) {
      //applypostloader('HIDE');
      $select.modal('close');
      if(data == 'checkuserauthclassnv') {
        checkuserauthclassnv();
      } else if(data == 'checkuserauthclassg') {
        checkuserauthclassg(); 
      } else {
        Materialize.toast('Published.', 2000, 'green');
        newct = 0;
        lastModified = [];
        storedFiles = [];
        storedFilesExsting = []; 
        storedFiles.length = 0;
        storedFilesExsting.length = 0;
        $(".empty_review_block").remove();
        $(data).hide().prependTo(".collection").fadeIn(3000);
        setTimeout(function() { 
          initDropdown();
        }, 400);
      } 
    }
  }).done(function(){
     lightGalleryinitialize();
      //resizefixPostImages();
    });
  return true;
}

function uploadphotoscamping(id, obj) {
    if($(obj).hasClass('checkuserauthclassnv')) {
        checkuserauthclassnv();
    } else if($(obj).hasClass('checkuserauthclassg')) {
        checkuserauthclassg();
    } else {
        if(id != undefined && id != '') {  
            $.ajax({ 
                url: '?r=camping/uploadphotoscamping',
                type: 'POST', 
                data: {id},
                success: function(data) {
                    $('#uploadphotosCampingModal').html(data);
                    setTimeout(function() { 
                        initDropdown();
                        $('#uploadphotosCampingModal').modal('open'); 
                    }, 400);
                }
            });
        }
    }
}

function uploadphotoscampingsave(id, obj) {
  applypostloader('SHOW');
  var validate = true;
  var fd;
  fd = new FormData();
  for(var i=0, len=storedFiles.length; i<len; i++) {
      fd.append('camping_images[]', storedFiles[i]);
  }
  fd.append('camping_id', id);

  if ($('#uploadphotosCampingModal').find('.custom-file').length) {
    validate = false;
    Materialize.toast('Please upload three cover photos.', 2000, 'red');
    applypostloader('HIDE');
    return false;
  } else if ($('#uploadphotosCampingModal').find('.post-photos').find('.img-row').find('.img-box').length != 3) {
    validate = false;
    Materialize.toast('Please upload three cover photos.', 2000, 'red');
    applypostloader('HIDE');
    return false;
  }
  
  if(validate) {
    $.ajax({
      url: '?r=camping/uploadphotoscampingsave',
      type: 'POST',
      data: fd,
      contentType: 'multipart/form-data',
      processData: false,
      contentType: false,
      success: function(result) {
        //applypostloader('HIDE');
        $result = JSON.parse(result);
        if($result.status != undefined && $result.status == true) {
          $('#uploadphotosCampingModal').modal('close'); 
          Materialize.toast('Camping updated.', 2000, 'green');
          window.location.href="";
        }
      }
    });
  }
}

function deleteCamping(nid, isPermission=false){
  if(isPermission) {
    $.ajax({
      url: '?r=camping/delete', 
      type: 'POST',
      data: {nid},
      success: function (data) {
        var result = $.parseJSON(data);
        if(result.status != undefined && result.status == true) {
          Materialize.toast('Camping deleted', 2000, 'green');
          window.location.href="";
        }
      }
    });
  } else {
    var disText = $(".discard_md_modal .discard_modal_msg");
    var btnKeep = $(".discard_md_modal .modal_keep");
    var btnDiscard = $(".discard_md_modal .modal_discard");
    disText.html("Delete camping.");
    btnKeep.html("Keep");
    btnDiscard.html("Delete");
    btnDiscard.attr('onclick', 'deleteCamping(\''+nid+'\', true)');
    $(".discard_md_modal").modal("open");
  }
}