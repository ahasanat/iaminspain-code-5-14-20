/* FUN signup navigation */
	var isRobotChecked = 'no';
	$(document).ready(function() {
		generate_captcha_code();
	});

	/*$(document).on('click', '#recaptchacodebox', function() {
		ischecked = $('#login_modal').find('.recaptcha-checkbox').attr('aria-checked')
	});*/
	
	function resetRegisterSteps(){
		$(".homebox.signup-box").hide();
	}
	
	function tbSignupNavigation(obj, cls){
		var dclass='';
		var objval='';

		if(cls!='' && cls!=undefined) {	
			dclass=cls;
		} else {
			dclass=$(obj).attr('data-class');
			objval=$(obj).html();
		}

		if(objval=="skip")
		{
		}
		else
		{ 
			if(dclass=="profile-setting"){
				var res = tbSignup();
				if(res == '1')
				{
					resetRegisterSteps();
					$("#"+dclass).show();
				}
			}
			if(dclass=="upload-photo"){
				var res = signup2();
				if(res == '1')
				{
					resetRegisterSteps();
					$("#"+dclass).show();
				}
			}
			if(dclass=="security-check") {
				resetRegisterSteps();
				$("#"+dclass).show();
			}
			if(dclass=="confirm-email"){ 
				var res = verifycount();
				if(res == '1')
				{
					accountVerify(1);
					resetRegisterSteps();
					$("#"+dclass).show();
				}
			}
		}		
	}
	
	function recaptchaCallback() {
		isRobotChecked = 'yes';
		if(isRobotChecked == 'yes') {
			$("#filled-in-box2").removeAttr("disabled");
			//$("#filled-in-box2").attr('checked', true);
		}
	}
	
	function resetForgotPassSteps(){
		$(".homebox.forgot-box").hide();
	}
	
	function setForgotPassStep(){
		$(".homebox.forgot-box").hide();
		$(".homebox.forgot-box#fp-step-1").show();
	}
	
	function forgotPassNavigation(obj){
		var dclass=$(obj).attr('data-class');
		var objval=$(obj).html();
		if(objval=="skip"){
			/* no validation check */
		}
		else
		{
			if(dclass == 'fp-step-2')
			{
				var forgotemail = $("#forgotemail").val();
				var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
				if(forgotemail === '')
				{
					$("#fpeml-success").hide();
					$("#fpeml-fail").show();
					Materialize.toast('Enter email address.', 2000, 'red');
					return false;
				}
				else if(!pattern.test(forgotemail))
				{
					$("#fpeml-success").hide();
					$("#fpeml-fail").show();
					Materialize.toast('Enter valid email address.', 2000, 'red');
					return false;
				}
				else
				{
					$.ajax({
					type: 'POST',
					url: '?r=site/check-forgot-email',
					data:'fpemail='+forgotemail,
					success: function(data){
							if(data)
							{
								if(data != '1')
								{
									Materialize.toast('Enter registered email address.', 2000, 'red');
									$("#fpeml-success").hide();
									$("#fpeml-fail").show();
									return false;
								}
								else
								{
									$("#fpeml-success").show();
									$("#fpeml-fail").hide();
									$.ajax({
										type: 'POST',
										url: '?r=site/forgotpassword',
										data: "forgotemail=" + forgotemail,
										success: function(data){
											 if(data)
											 {
												var domain = forgotemail.substring(forgotemail.lastIndexOf("@") +1);
												if(domain === 'gmail.com'){domain = 'http://mail.google.com';}
												else if(domain === 'hotmail.com'){domain = 'https://mail.live.com/m/';}
												else{domain = 'http://mail.'+domain+'/'}
												$("#displayresetlink").html('<a href="'+domain+'" class="homebtn autow">Check Email</a>');
												Materialize.toast('Check your email address.', 2000, 'green');
												resetForgotPassSteps();
												$("#"+dclass).show();
											 }
											 else
											 {
											 	Materialize.toast('Enter with registered email address.', 2000, 'red');
											 	return false;
											 }
										 }
									});
								}
							}
							else
							{
								$("#fpeml-success").hide();
								$("#fpeml-fail").show();
								return false;
							}
						}
					});
				}
			}
			if(dclass == 'fp-step-4')
			{
				var fppassword = $("#fppassword").val();
				var fppasswordcon = $("#fppasswordcon").val();
				var travid = $("#travid").val();
				if(!(tbFp() && tbConFp()))
				{
					if (fppassword.length < 6)
					{
						Materialize.toast('Enter password of atleast 6 characters.', 2000, 'red');
						$("#fppassword").focus();
						return false;
					}
					else if (fppasswordcon.length < 6)
					{
						Materialize.toast('Enter password of atleast 6 characters.', 2000, 'red');
						$("#fppasswordcon").focus();
						return false;
					}
					else if(fppassword != fppasswordcon)
					{
						Materialize.toast('Password mismatched.', 2000, 'red');
						$("#fppasswordcon").focus();
						return false;
					}
					else
					{	
						return false;
					}
				}
				else
				{
					if(fppassword != '' && fppasswordcon != ''){
						$.ajax({
						   type: 'POST',
						   url: '?r=site/resetpassworddone',
						   data: "travid=" + travid +"&password=" + fppassword,
						   success: function(data){
								if(data)
								{
									$("#fp-step-3").hide();
									$("#"+dclass).show();
								}
								else
								{
									$('.fp-error').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
								}
							}

						}); 
					}
				}
			}
		}
	}

	function validateforgotemail(){
		var forgotemail = $("#forgotemail").val();
		var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
		if(forgotemail === '')
		{
			$("#fpeml-success").hide();
			$("#fpeml-fail").show();
			return false;
		}
		else if(!pattern.test(forgotemail))
		{
			$("#fpeml-success").hide();
			$("#fpeml-fail").show();
			return false;
		}
		else
		{
			$.ajax({
			type: 'POST',
			url: '?r=site/check-forgot-email',
			data:'fpemail='+forgotemail,
			success: function(data){
					if(data)
					{
						if(data != '1')
						{
							$("#fpeml-success").hide();
							$("#fpeml-fail").show();
							return false;
						}
						else
						{
							$("#fpeml-success").show();
							$("#fpeml-fail").hide();
							return true;
						}
					}
					else
					{
						$("#fpeml-success").hide();
						$("#fpeml-fail").show();
						return false;
					}
				}
			});
		}
	}
/* FUN end signup navigation */


/* home page functions */
	function validate_lemail(){
		var email = $("#lemail").val();
		var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
		if(email == "")
		{
			$("#leml-success").hide();
			$("#leml-fail").hide();
			$("#lemail").focus();
			return false;
		}
		if(!pattern.test(email)){
			$("#leml-success").hide();
			$("#leml-fail").show();
			var eml = $("#lemail").focus();
			if(eml == null) {
				return false;
				
			}
		}
		else{
			$.ajax({
				type: 'POST',
				url: '?r=site/check-email',
				data:'lemail='+email,
				success: function(data){
					if(data)
					{
					   $("#leml-success").show();
					   $("#leml-fail").hide();
					}
					else
					{
					  
						$("#leml-success").show();
						$("#leml-fail").hide();
					}
				}
			});
		}   
	}
	function validate_lpassword(){
		var lpassword = $("#lpassword").val();
		if(lpassword == ""){
			$("#lpwd-success").hide();
			$("#lpwd-fail").hide();
			$("#lpassword").focus();
			return false;
		}
		if(lpassword.length < 6){
			$("#lpwd-success").hide();
			$("#lpwd-fail").show();
			$("#lpassword").focus();
			return false;
		}
		else{
			$("#lpwd-fail").hide();
			$("#lpwd-success").show();
			return true;
		}
	}
	function validate_fname(){
		var fname = $("#fname").val();
		var reg_nm = /^[a-zA-Z\s]+$/;
		var spc = /^\s+$/;	
		if(fname == "")
		{	
			  
			$("#fnm-success").hide();
			$("#fnm-fail").hide();
			$("#fname").focus();		
			return false;
		}
		if(fname.length < 2){
			
			
			$("#fnm-success").hide();
			$("#fnm-fail").show();
			$("#fname").focus();
			return false;
		}	
		if(spc.test(fname)){
			$("#fnm-success").hide();
			$("#fnm-fail").show();
			$("#fname").focus();
			return false;
		}	
		if(!reg_nm.test(fname)){	
			
			$("#fnm-success").hide();
			$("#fnm-fail").show();
			$("#fname").focus();
			return false;
		}
		else{
			$("#fnm-fail").hide();
			$("#fnm-success").show();
			return true;
		}
	}

	function validate_fname1(){
		var fname = $("#fname").val();
		var reg_nm = /^[a-zA-Z\s]+$/;
		var spc = /^\s+$/;	
		if(fname == "")
		{
			Materialize.toast('Enter first name.', 2000, 'red');
			$("#fnm-success").hide();
			$("#fnm-fail").hide();
			$("#fname").focus();		
			return false;
		}
		if(fname.length < 2){
			Materialize.toast('Enter minimum 2 characters in first name.', 2000, 'red');
			$("#fnm-success").hide();
			$("#fnm-fail").show();
			$("#fname").focus();
			return false;
		}	
		if(spc.test(fname)){
			$("#fnm-success").hide();
			$("#fnm-fail").show();
			$("#fname").focus();
			return false;
		}	
		if(!reg_nm.test(fname)){	
			Materialize.toast('Enter characters only in first name.', 2000, 'red');	
			$("#fnm-success").hide();
			$("#fnm-fail").show();
			$("#fname").focus();
			return false;
		}
		else{
			$("#fnm-fail").hide();
			$("#fnm-success").show();
			return true;
		}
	}
	
	function validate_lname(){    
		var lname = $("#lname").val();
		var reg_nm = /^[a-zA-Z\s]+$/;
		var spc = /^\s+$/;	
		if(lname ==""){
			
			$("#lnm-success").hide();
			$("#lnm-fail").hide();
			$("#lname").focus();
		    $("#lname").focus();
			return false;
		}
		if(lname.length < 2){	
			
			$("#lnm-success").hide();
			$("#lnm-fail").show();
			$("#lname").focus();
			return false;
		}			
		if(spc.test(lname)){
			$("#lnm-success").hide();
			$("#lnm-fail").show();
			$("#lname").focus();
			return false;
		}
		if(!reg_nm.test(lname)){

			$("#lnm-success").hide();
			$("#lnm-fail").show();
			$("#lname").focus();
			return false;
		}
		else{
		  $("#lnm-fail").hide();
		  $("#lnm-success").show();
		  return true;
		}	   
	}
	
	function validate_lname1(){    
		var lname = $("#lname").val();
		var reg_nm = /^[a-zA-Z\s]+$/;
		var spc = /^\s+$/;	
		if(lname ==""){
			Materialize.toast('Enter last name.', 2000, 'red');	
			$("#lnm-success").hide();
			$("#lnm-fail").hide();
			$("#lname").focus();
		    $("#lname").focus();
			return false;
		}
		if(lname.length < 2){	
			Materialize.toast('Enter minimum 2 characters in last name.', 2000, 'red');
			$("#lnm-success").hide();
			$("#lnm-fail").show();
			$("#lname").focus();
			return false;
		}			
		if(spc.test(lname)){
			$("#lnm-success").hide();
			$("#lnm-fail").show();
			$("#lname").focus();
			return false;
		}
		if(!reg_nm.test(lname)){
			Materialize.toast('Enter characters only in last name.', 2000, 'red');
			$("#lnm-success").hide();
			$("#lnm-fail").show();
			$("#lname").focus();
			return false;
		}
		else{
		  $("#lnm-fail").hide();
		  $("#lnm-success").show();
		  return true;
		}	   
	}
	
	function validate_email(){
		var email = $("#email").val().trim();
		var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
		if(email =="") {
			Materialize.toast('Enter email.', 2000, 'red');
			$("#eml-fail").hide();
			$("#eml-success").hide();
			$("#email").focus();
			return false;
		}
		if(! pattern.test(email)){
			Materialize.toast('Enter valid email.', 2000, 'red');
			$("#eml-success").hide();
			$("#eml-fail").show();
			$("#email").focus();
			return false;
		}
		else{
			$("#eml-fail").hide();
			$("#eml-success").show();
			return true;
		}   
	}
	
	function validate_email1(){
		var email = $("#email").val().trim();
		var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
		if(email =="") {
			Materialize.toast('Enter email.', 2000, 'red');
			$("#eml-fail").hide();
			$("#eml-success").hide();
			$("#email").focus();
			return false;
		}
		if(! pattern.test(email)){
			Materialize.toast('Enter valid email.', 2000, 'red');
			$("#eml-success").hide();
			$("#eml-fail").show();
			$("#email").focus();
			return false;
		}
		else{
			$("#eml-fail").hide();
			$("#eml-success").show();
			return true;
		}   
	}

	function validate_spassword(){
		var spassword = $("#signup_password").val();
		
		if(spassword =="")
		{		
			$("#spwd-fail").hide();
			$("#spwd-success").hide();
			$("#signup_password").focus();
			return false;
		}
		if(spassword.length < 6)
		{		
			$("#spwd-success").hide();
			$("#spwd-fail").show();
			return false;
		}
		else{
			$("#spwd-fail").hide();
			$("#spwd-success").show();
			return true;
		}		
	}
	 
	function validate_spassword1(){
		var spassword = $("#signup_password").val();
		
		if(spassword =="")
		{			
			Materialize.toast('Enter password.', 2000, 'red');
			$("#spwd-fail").hide();
			$("#spwd-success").hide();
			$("#signup_password").focus();
			return false;
		}
		if(spassword.length < 6)
		{		
			Materialize.toast('Enter password of atleast 6 characters.', 2000, 'red');
			$("#spwd-success").hide();
			$("#spwd-fail").show();
			return false;
		}
		else{
			$("#spwd-fail").hide();
			$("#spwd-success").show();
			return true;
		}		
	}
	
	function tbFp(){
		var fppassword = $("#fppassword").val();
		if(fppassword == ""){
			Materialize.toast('Enter password.', 2000, 'red');
			$("#pwd-fail").hide();
			$("#pwd-success").hide();
		    $("#fppassword").focus();
			return false;
		}
		if(fppassword.length < 6){
			Materialize.toast('Enter password of atleast 6 characters.', 2000, 'red');
			$("#pwd-success").hide();
			$("#pwd-fail").show();
			return false;
		}
		else{
			$("#pwd-fail").hide();
			$("#pwd-success").show();
			return true;
		}
	}
	function tbConFp(){
		var fppassword = $("#fppassword").val();
		var fppasswordcon = $("#fppasswordcon").val();
		if(fppasswordcon ==""){
			$("#conpwd-fail").hide();
			$("#conpwd-success").hide();
			$("#fppasswordcon").focus();
			
			return false;
		}
		if(fppasswordcon.length < 6){
			$("#conpwd-success").hide();
			$("#conpwd-fail").show();
			return false;
		}
		else if(fppassword != fppasswordcon){
			$("#conpwd-success").hide();
			$("#conpwd-fail").show();
			return false;
		}
		else{
			$("#conpwd-fail").hide();
			$("#conpwd-success").show();
			return true;
		}
	}
	function validate_city(){
		var city = $("#autocomplete").val();
		if(city == ""){			
			Materialize.toast('Enter city.', 2000, 'red');
			$("#autocomplete").focus();			
			return false;
		}
		return true;
	} 
	function checkAvailability() {

		var phone = $("#phone").val();
		var ptn = /([0-9\s\-]{1,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;
	   
		if(phone ==""){
			Materialize.toast('Enter mobile number.', 2000, 'red');
			$("#phn-success").hide();
			$("#phn-fail").show();
			$("#phone").focus();
			return false;			
		}
		else if(!ptn.test(phone)){
			Materialize.toast('Enter valid number.', 2000, 'red');
			$("#phn-success").hide();
			$("#phn-fail").show();
			$("#phone").focus();
			return false;
		}
		else{
			$.ajax({
				url: "?r=site/phone",
				data:'phone='+$("#phone").val(),
				type: "POST",
				success:function(data){
					if(data == '1')
					{
						$("#phn-fail").hide();
						$("#phn-success").show();
						return true;
					}
					else
					{
						$("#phn-success").hide();
						$("#phn-fail").show();
						return false;
					}
				},				
			});
		}
	}
	function gender_status(gender){
		$('#genderstatus').val(gender);
	}
	function changedate(){
		var adate = $('#popupDatepicker').val();
		$('#birthdate').val(adate);
	}
    function accountVerify(t){

		if(t == 2) {
			$(".notice1 .post_loadin_img").css("display","inline-block");   
		}
		else if(t == 1){
			$(".notice .post_loadin_img").css("display","inline-block");
		}
	  
		var email = $("#user_email").val();  
	 
		if(email != ''){
			$.ajax({
				type: 'POST',
				url: '?r=site/verify',
				data: "email=" + email,
				success: function(data){
				   
					if(data){
						if(t == 2) {
							Materialize.toast('Mail Sent Successfully Please Verify Your Account From Your Email Id', 2000, 'red');
						}
					}
					else
					{
						if(t == 2) {		  
							Materialize.toast('Oops..!! Somthing Went Wrong.', 2000, 'red');
						}
					}
				}
			});
		}
	}
	function tbSignup(){
		var bfname = $("#fname").val();
		var blname = $("#lname").val();
		var email = $("#email").val().trim();
		var password = $("#signup_password").val();
		var fname = firstToUpperCase( bfname );
		var lname = firstToUpperCase( blname );
		
		$("#uname").html(fname + ' ' +lname);
		
		$("#user_email").val(email);
		
	
		$("#fullname").val(fname+ " "+lname);
		$("#fname").val(fname);
		$("#lname").val(lname);
		
		if(!(validate_fname1() && validate_lname1() && validate_email1() && validate_spassword1())){
			Materialize.toast('Request deleted.', 2000, 'red');
			return false;			 
		}else{
			var $data='';
			$.ajax({
			   type: 'POST',
			   url: '?r=site/signup',
			   data: $("#frm").serialize(),
			   async: false,
			   success: function(data){
					var domain = email.substring(email.lastIndexOf("@") +1);
					 if(domain === 'gmail.com'){domain = 'http://mail.google.com';}
					 else if(domain === 'hotmail.com'){domain = 'https://mail.live.com/m/';}
					 else{domain = 'http://mail.'+domain+'/'}
					 $("#confirmlink").html('<a href="'+domain+'" class="homebtn autow">Confirm Email</a>');
				   $data = data;
			   }			  
		   });
			if($data == '0'){
				return false;
			}
			else if($data == '1'){ 
				return true;
			}
			else if($data == '5'){
				Materialize.toast('Email address already registered.', 2000, 'red');
				return false;
			}
			else if($data == '6'){
				Materialize.toast('Email address already registered.', 2000, 'red');
				return false;
			}
			else{
				return false;
			}
		}
	}
/* end home page functions */

/* Start Function Signup2 */ 
	function signup2(){
		
		var datepick = $("#datepicker").val();
		var gender = $("#gender").val();
		var country = $("#country").val();
		$("#ucountry").html(country);
		
		if(!(validate_city())){
			return false;
		}
		else if(gender != 'Male' && gender != 'Female'){
			Materialize.toast('Select gender.', 2000, 'red');
			$("#gender").focus();
			return false;
		}
		else if(datepick == ''){
			Materialize.toast('Select birthdate.', 2000, 'red');
			$("#datepicker").focus();
			return false;
		}
		else{
			var $data='';
			$.ajax({
			   type: 'POST',
			   url: '?r=site/signup2',
			   data: $("#frm2").serialize(),
			   async: false,
			   success: function(data){
				   $data = data;
			   }
		   });
			if($data == '0')
			{
				 return false;
			}
			else
			{
				return true;
			}
		}
	}
/* End Function Signup2 */ 
	
/* Start Function Generate_CaptchaCode */
	function generate_captcha_code(){
		var rand1 = Math.floor(Math.random() * 8)+1;
		var rand2 = Math.floor(Math.random() * 8)+1;
		$(".blabel").html(rand1+' + '+rand2+' = ');
	}
/* End Start Function Generate_CaptchaCode */

/* check captcha */ 
	function verifycount() {
		if(isRobotChecked == 'no') {
		    //if($("#filled-in-box").prop('checked') == false) {
			//$("#filled-in-box2").attr("disabled", true);
			Materialize.toast('Checked I am not robot.', 2000, 'red');
			return false;
		} else if($("#filled-in-box2").prop('checked') == false) {
			$("#filled-in-box2").removeAttr("disabled");
			Materialize.toast('Agree terms and conditions.', 2000, 'red');
			return false;
		} else {
			//$("#filled-in-box").removeAttr("disabled");
			$("#filled-in-box2").removeAttr("disabled");
			//$("#filled-in-box").attr('checked', true);
			$("#filled-in-box2").attr('checked', true);
			return true;
		}
	}	
/* end check captcha */ 
	
/* login button */
	function check_enter(obj, e){
		if (e.which == 13){
			tbLogin();
			return false;
		}	
	}
	function tbLogin(){
		var lemail = $("#lemail").val();
		var lpassword = $("#lpassword").val();
		var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
		if(lemail === '' && lpassword === '')
		{
			Materialize.toast('Enter email address and password.', 2000, 'red');
			$("#lemail").focus();
		}
		else if(lemail === '')
		{
			Materialize.toast('Enter email address.', 2000, 'red');
			$("#lemail").focus();
		}
		else if(!pattern.test(lemail))
		{
			Materialize.toast('Enter valid email address.', 2000, 'red');
			$("#lemail").focus();
		}
		else if(lemail !== '' && lpassword === '')
		{
			Materialize.toast('Enter password.', 2000, 'red');
			$("#lpassword").focus();
		}
		else
		{
			$.ajax({
				type: 'POST', 
				url: '?r=site/check-login',
				data:'lemail='+lemail+"&lpassword="+lpassword,
				success: function(data){
					if(data)
					{
						var result = $.parseJSON(data);
						lat = '';
						long = '';

						if(result['value'] == '2')
						{
							Materialize.toast('Login with registered email address.', 2000, 'red');
						}
						else if(result['value'] == '3')
						{
							Materialize.toast('Enter correct password.', 2000, 'red');
						}
						else if(result['value'] == '4')
						{
							if($('#lat').length) {
								lat = $('#lat').val();
							}
							if($('#long').length) {
								long = $('#long').val();
							}
							
							$.ajax({
								type: 'POST', 
								url: '?r=site/extra-login',
								data:{
									'email' : lemail,
									'password' : lpassword,
									'lat' : lat,
									'long' : long,
									'login' : 'yes'
								},
								success: function(data){
									var result = $.parseJSON(data);
									if(result['result'] != undefined && result['result'] == true && result['cango'] == 'yes') {
										$block = result['block'];
										$link = result['link'];
										if($link != '?r=site/complete-profile') {
											$('#Complete_loged').find('.signup-part').show();
											$('#wlcmsrnbx').html($block);
											$('#Complete_loged').modal('open');
										}
										window.location.href= $link;

									} else {
										if(result['reason'] != undefined) {
											$reason = result['reason'];
										} else {
											$reason = result['reason'];
										}

										Materialize.toast($reason, 2000, 'Oops, somthing is wrong please try after sometimes');
									}
								}
							});
						}
						else if(result['value'] == '10')
						{
							if($('#lat').length) {
								lat = $('#lat').val();
							}
							if($('#long').length) {
								long = $('#long').val();
							}
							
							$.ajax({
								type: 'POST', 
								url: '?r=site/extra-login',
								data:{
									'email' : lemail,
									'password' : lpassword,
									'lat' : lat,
									'long' : long,
									'login' : 'yes'
								},
								success: function(data){
									var result = $.parseJSON(data);
									if(result['result'] != undefined && result['result'] == true && result['cango'] == 'yes') {
										$block = result['block'];
										$link = result['link'];
										$('#Complete_loged').find('.signup-part').show();
										$('#wlcmsrnbx').html($block);
										$('#Complete_loged').modal('open');
										window.location.href= $link;
									} else {
										if(result['reason'] != undefined) {
											$reason = result['reason'];
										} else {
											$reason = result['reason'];
										}

										Materialize.toast($reason, 2000, 'Oops, somthing is wrong please try after sometimes');
									}
								}
							});
						}
						else if(result['value'] == '5')
						{
							Materialize.toast('Check ur credentials.', 2000, 'red');
						}
						else if(result['value'] == '7')
						{
							Materialize.toast('Check ur credentials.', 2000, 'red');
						}
						else
						{

						}
					}
					else
					{
					}
				}
			});
		}
	}		

	function doLogin() {

	}
/* end login button */
 