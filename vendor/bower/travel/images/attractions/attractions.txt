{
    "name" : "LAS VEGAS STRIP",
    "city" : "LAS VEGAS",
    "peak_season" : "",
    "description" : "It's hard t miss the dancing Bellagio fountains, and why would you want to? Some warm clothes are advised, although it's scorching outside, the casinos are famous for their chilly AC policies.",
    "profile" : "Las Vegas.jpg",
    "visitors" : '39,668,221',
    "rank" : 1,
    "created_at" : "1505114195",
    "modified_at" : "",
    "status" : 1
}
{
    "name" : "TIMES SQUARE",
    "city" : "NEW YORK CITY",
    "peak_season" : "JAN-MAR,JUN-AUG",
    "description" : "A treasure-trove of LED lighUng and attrachons. Go on a walking Was first or do some research. horn, so Mat you can choose between the abundance of theatre. music and comedy shows.",
    "profile" : "Times-square.jpg",
    "visitors" : '39,200,000',
    "rank" : 2,
    "created_at" : "1505114195",
    "modified_at" : "",
    "status" : 1
}
{
	"name" : "CENTRAL PARK",
	"city" : "NEW YORK CITY",
	"peak_season" : "JAN-MAR,JUN-AUG",
	"description" : "Attend a concert or pay In the summet, or don your blades in the winter for a spot of ice skating on either Me the Wollman or Laser rinks.",
	"profile" : "Central Park.jpg",
	"visitors" : '37,500,000',
	"rank" : 3,
	"created_at" : "1505114195",
	"modified_at" : "",
	"status" : 1
}
{
    "name" : "UNION STATION",
    "city" : "WASHINGTON D.C",
    "peak_season" : "MAR-JUN, SEPT-NOV",
    "description" : "After basking fn Me wonder of the architecture. why not partake In one of the many city tours based at the station ?",
    "profile" : "Washington DC.jpg",
    "visitors" : '32,850,000 ',
    "rank" : 4,
    "created_at" : "1505114195",
    "modified_at" : "",
    "status" : 1
}

{
    "name" : "NIAGARA FALLS",
    "city" : "Ontario, Canada",
    "peak_season" : "JUN-AUG",
    "description" : "Covering its passengers M the dense mist of Me falls. the Maid of Me service has been running since 1846. You will face larger crowds in the peak season but .1 be greeted by warmer weather.",
    "profile" : "Niagara_Falls.jpg",
    "visitors" : '22,500,000 ',
    "rank" : 5,
    "created_at" : "1505114195",
    "modified_at" : "",
    "status" : 1
}

{
    "name" : "GRAND CENTRAL TERMINAL",
    "city" : "NEW YORK CITY",
    "peak_season" : "JAN-MAR,JUN-AUG",
    "description" : "The restored astronomical celling is a highlight. be sure to check out Me market and oyster bar too.",
    "profile" : "grand_central_terminal.jpg",
    "visitors" : '21,600,000 ',
    "rank" : 6,
    "created_at" : "1505114195",
    "modified_at" : "",
    "status" : 1
}

{
    "name" : "FANEUIL HALL MARKETPLACE",
    "city" : "BOSTON",
    "peak_season" : "APR-JUN,SEPT-OCT",
    "description" : "The backdrop of many famous peeches. this marketplace also has an array of dining and and shopping options. Stop to watch a street performer or check out one of the many entertainmeM and music events.",
    "profile" : "faneuil hall marketplace.jpg",
    "visitors" : '18,000,000 ',
    "rank" : 7,
    "created_at" : "1505114195",
    "modified_at" : "",
    "status" : 1
}

{
    "name" : "DISNEYWORLD'S MAGIC KINGDOM",
    "city" : "ORLANDO",
    "peak_season" : "FEB-APR",
    "description" : "6 regions and 140 main rides within a single park, dive into the sea with the Little Mermaid and fly into space with Buzz Lightyear.",
    "profile" : "disney magic world.jpg",
    "visitors" : '17,536,000 ',
    "rank" : 8,
    "created_at" : "1505114195",
    "modified_at" : "",
    "status" : 1
}

{
    "name" : "DISNEYLAND PARK",
    "city" : "ANAHEIM",
    "peak_season" : "",
    "description" : "TheSpace and Splash Mountain rides delight thrill-seekers but Mere are plenty of attractions for youngsters here too.",
    "profile" : "disneyland_park.jpg",
    "visitors" : '15,963,000',
    "rank" : 9,
    "created_at" : "1505114195",
    "modified_at" : "",
    "status" : 1
}
{

    "name" : "FORBIDDEN CITY",
    "city" : "BEIJING",
    "peak_season" : "WEEKENDS CHINESE HOLIDAYS",
    "description" : "For 500 years. this meticulously planned maze of traditional buildings was the centre of Chinese government and has been the home of many emperors. Opened to the public in 1925. it contains some of the world's finest artefacts.",
    "profile" : "beijing forbidden.jpg",
    "visitors" : '15,300,000',
    "rank" : 10,
    "created_at" : "1505114195",
    "modified_at" : "",
    "status" : 1
}

{

    "name" : "GRAND BAZAAR",
    "city" : "ISTANBUL",
    "peak_season" : "JUN-AUG",
    "description" : "61 covered streets and over 3000 shops In what is an ancient and almost mythical covered market. Sit down and drink sweet apple tea with traders, while bartering over exotic and curious goods.",
    "profile" : "beijing forbidden.jpg",
    "visitors" : '15,000,000',
    "rank" : 11,
    "created_at" : "1505114195",
    "modified_at" : "",
    "status" : 1
}

{

    "name" : "TOKYO DISNEYLAND",
    "city" : "TOKYO",
    "peak_season" : "MAR-AUG",
    "description" : "A taste of Me west in the east, this symbol of globalisation will thrill adulte and delight children in equal measure.",
    "profile" : "tokyo disneyland.jpg",
    "visitors" : '14,847,000',
    "rank" : 12,
    "created_at" : "1505114195",
    "modified_at" : "",
    "status" : 1
}