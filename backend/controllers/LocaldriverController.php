<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use frontend\models\UserForm;
use frontend\models\Vip;
use backend\models\AddvipPlans; 
use backend\models\AddcreditsPlans;
use backend\models\Userdata;
use backend\models\BusinessCategory;
use backend\models\TravstoreCategory;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use frontend\models\Slider;
use frontend\models\Cover;
use frontend\models\Language;
use frontend\models\Education;
use frontend\models\Interests;
use frontend\models\Occupation;
use frontend\models\Notification;
use frontend\models\Page;
use frontend\models\PostForm;
use backend\models\Endorsement;
use backend\models\Addgiftimages;
use backend\models\LocaldriverActivity;
use backend\models\LocaldriverPost;
/**
 * Site controller
 */
class LocaldriverController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
         return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['localdriverinformation','localdriverabuse','godelete','godeleteabuse','activity','addactivity'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],		
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionLocaldriverinformation() {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if(isset($uId)) {
            $information = LocaldriverPost::getLocaldriverList($uId);
            return $this->render('localdriverinformation', array('information' => $information));
            exit;
        }
    }

    public function actionLocaldriverabuse() {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if(isset($uId)) {
            $information = LocaldriverPost::abuseReport($uId);
            return $this->render('abuse', array('information' => $information));
            exit;
        }
    }

    public function actionGodelete() {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if(isset($uId)) {
            if(isset($_POST['$id']) && $_POST['$id'] != '') {
                $id = $_POST['$id'];
                $information = LocaldriverPost::godelete($uId, $id);
                return $information;
                exit;
            }
        }
    } 

    public function actionGodeleteabuse() {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if(isset($uId)) {
            if(isset($_POST['$id']) && $_POST['$id'] != '') {
                $id = $_POST['$id'];
                $information = LocaldriverPost::godeleteabuse($uId, $id);
                return $information;
                exit;
            }
        }
    }

    public function actionActivity() {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if($uId) {
            $information = LocaldriverActivity::getallactivity();
            return $this->render('activity', array('information' => $information));
            exit;
        }
    }

    public function actionAddactivity() {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if($uId) {
            if(isset($_POST['$name']) && $_POST['$name'] != '') {
                $name = $_POST['$name'];
                $information = LocaldriverActivity::addactivity($uId, $name);
                return $information;
                exit;
            }
        }
    }
}
