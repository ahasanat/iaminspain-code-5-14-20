<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Host Listing';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
$isLast='';
if(isset($information) && !empty($information)) {
	$information = json_decode($information, true);
	if(!empty($information)) {
		$isLast = (string)$information[0]['price'];
	}
} else {
	$information = array();
} 

?>
<div class="content-wrapper messagepage"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
    	<h1>Gift</h1> 
	</section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box sticker-loader">
					<div class="box-header">
			            <div class="box-header">
			              	<h3 class="box-title">Update Gift Cost</h3>
			            </div>
			            <!-- /.box-header --> 
			            <div class="box-body">
			              	<div>
		                      <div class="row">
		                        <div class="emotionstickers">
		                          
		                        </div>
		                      </div>
		                      <!-- /.row -->
		                    </div>
			            </div>
			            <!-- /.box-body -->
			          </div>
				</div>
			</div>
		</div>
    </section>
</div>
 
<script>
$(document).ready(function() {
	setTimeout(function() {
		var $faces = ['(wine)','(icecream)','(coffee)','(heart)','(flower)','(cake)','(handshake)','(gift)','(goodmorning)','(goodnight)','(backpack)','(parasailing)','(train)','(flipflop)','(airplane)','(sunbed)','(happyhalloween)','(merrychristmas)','(eidmubarak)','(happyyear)','(happymothers)','(happyfathers)','(happyaniversary)','(happybirthday)'];
		$('.emotionstickers').html('');
		$.each($faces, function(i, v){
			var faces = $.emostickers.replace(v);			
			$('.emotionstickers').append('<div class="col-sm-4">'+faces+'</div>');
		});
	}, 2000);
});
</script>
