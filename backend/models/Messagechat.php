<?php

namespace backend\models;

use yii\base\Model;
use Yii;
use yii\mongodb\ActiveRecord;
use frontend\models\UserForm;
/**
 * This is the model class for table "session_frontend_user".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $ip 
 * @property integer $expire
 * @property resource $data
 */
class Messagechat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'message_gift';
    }

    public function attributes() {
        return ['_id', 'type', 'image_name', 'created_at'];
    }

    public function IsAdmin($id) {
        if($id) {
            $result = Admin::findOne($id);
            if($result) {
                return true;
                exit;
            }
        } 

        return exit;
        exit;
    }

}